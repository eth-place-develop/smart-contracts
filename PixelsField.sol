pragma solidity ^0.4.18;


/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable {
  address public owner;


  event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);


  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  function Ownable() public {
    owner = msg.sender;
  }


  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }


  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function transferOwnership(address newOwner) public onlyOwner {
    require(newOwner != address(0));
    OwnershipTransferred(owner, newOwner);
    owner = newOwner;
  }

}

/**
 * @title Controllable
 * @dev The Controllable contract has an controller address, and provides basic control.
 */
contract Controllable is Ownable {
  address public controller;
  
  event ControllerChanged(address indexed previousController, address indexed newController);

  /**
   * @dev Allows the current owner to set new controller address.
   * @param newController the address of new controller.
   */    
  function setController(address newController)  public onlyOwner {
    ControllerChanged(controller, newController);
    controller = newController;
  }
    
  /**
   * @dev Throws if called by any account other than the controller.
   */
  modifier onlyController() {
    require(msg.sender == controller);
    _;
  }
}

/**
 * 
 * @title PixelsField
 * @dev The PixelsField contract keep pixels colors.
 */
contract PixelsField is Controllable {
  
    event PixelChanged(uint coordinates, uint clr);
    
    uint[15625] public colors;
    
    uint bitMask;
    uint n = 4;
    uint ratio = 64;
    
    function PixelsField() public {
        bitMask = (uint8(1) << n) - 1;
    }
    
    function setPixel(uint coordinate, uint color) public onlyController {
        require(color < 16);
        require(coordinate < 1000000);

        uint idx = coordinate / ratio;
        uint bias = coordinate % ratio;
        
        uint old = colors[idx];
        uint zeroMask = ~(bitMask << (n * bias));
        colors[idx] = (old & zeroMask) | (color << (n * bias));
        
        PixelChanged(coordinate, color);
    }
    
    function getPixel(uint coordinate) public view returns (uint) {
        var idx = coordinate / ratio;
        var bias = coordinate % ratio;
        return (colors[idx] >> (n * bias)) & bitMask;
    }
    
    function allPixels() public view returns (uint256[15625]) {
        return colors;
    }
}
