pragma solidity ^0.4.18;

contract PixelsField {
  function setPixel(uint coordinate, uint color) public;
}


contract PixelsController {
    address public owner;
    PixelsField public field = PixelsField(0x97455EfBdDdC5F0dc74EEEB368328BD99f626B57);
    
    function PixelsController() public {
        owner = msg.sender;
    }
    
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
  
    function changeFieldAddr(address newAddr) public onlyOwner {
        field = PixelsField(newAddr);
    }
    
    function setPixel(uint coordinates, uint color) public {
        field.setPixel(coordinates, color);
    }
}